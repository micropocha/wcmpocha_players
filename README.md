Description
==================================

This component is intended to handle the amount of players playin micropocha

## attributes (html):

- amount: (number) number of players

## events

- playerAdd (number, name?)

## listens

- playerAdd (number, name?)


Run
=======================================

## run first time

```bash
npm install 
npm start
```

## test

```bash
npm test
```