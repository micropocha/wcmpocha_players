(async () => {
  class wcmPochaPlayers extends HTMLElement {
    constructor() {  
      super();
      // Init default variables 
      this._amount = 0;
      // create shadowdoom
      this._root = this.attachShadow({mode: 'closed'});
      this._root.appendChild(this.createBody());
      this._root.appendChild(this.createStyle());

    }

    createBody(){
      const markup = `
      <span class="myclass">
        <em>number of players:</em> 
        <span id="amount">${this._amount}</span>
        <button id="addbutton">Add</button>
      </span>`;
      var wrapper = document.createElement("div")
      wrapper.innerHTML = markup;
      return wrapper;
    }

    createStyle(){
      var style = document.createElement('style'); 
      style.textContent = `
      .myclass {
        color: black;
      }`;
      return style;
    }

    // Getter to let component know what attributes
    // to watch for mutation in this example: country
    static get observedAttributes() {
      return ['amount']; 
    };

    // Fires when an attribute was added, removed, or updated (only for listened in observedAttributes)
    attributeChangedCallback (attr, oldVal, newVal) {
      console.log(`${attr} was changed from ${oldVal} to ${newVal}!`);
      if ("amount" == attr) { 
        if (newVal) this._amount = Number(newVal);
        this._root.getElementById("amount").textContent = this._amount; // you can move this line to invalidate()

      }
    }

    // Called when element is inserted in DOM
    connectedCallback() {
      this._root.getElementById("addbutton").addEventListener("click", e => {
        this.firePlayerAddEvent();
      });
      window.addEventListener('playerAdd', e => {
        this.setAttribute("amount", e.detail.number);
      })
    };

    firePlayerAddEvent(){
      var payload = {number: this._amount + 1 , name: null};
      this.dispatchEvent(new CustomEvent('playerAdd', {detail: payload, bubbles: true}))
    }
  }
  
  customElements.define('wcmpocha-players', wcmPochaPlayers);
})();